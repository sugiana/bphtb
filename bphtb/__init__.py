from pyramid.config import Configurator
from sqlalchemy import engine_from_config
from views import (
    RpcBPHTB,
    RpcPBB,
    RpcPADL,
    RpcAuth,
    )
from .models import (
    DBSession,
    PbbDBSession,
    PadlDBSession,
    AuthDBSession,
    Base,
    PbbBase,
    PadlBase,
    )


def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    if settings['sqlalchemy.url']:
        engine = engine_from_config(settings, 'sqlalchemy.')
        DBSession.configure(bind=engine)        
        Base.metadata.bind = engine
    if settings['pbb_sqlalchemy.url']:
        pbb_engine = engine_from_config(settings, 'pbb_sqlalchemy.')
        PbbDBSession.configure(bind=pbb_engine)        
        PbbBase.metadata.bind = pbb_engine
    if settings['padl_sqlalchemy.url']:
        padl_engine = engine_from_config(settings, 'padl_sqlalchemy.')
        PadlDBSession.configure(bind=padl_engine)
        PadlBase.metadata.bind = padl_engine        
    auth_engine = engine_from_config(settings, 'auth_sqlalchemy.')    
    AuthDBSession.configure(bind=auth_engine)
    config = Configurator(settings=settings)
    config.include('pyramid_chameleon')
    config.add_static_view('static', 'static', cache_max_age=3600)
    config.add_static_view('files', settings['static_files'])    
    config.add_route('home', '/')
    config.add_view(RpcBPHTB, name='bphtb')
    config.add_view(RpcPBB, name='pbb')
    config.add_view(RpcPADL, name='padl')
    config.add_view(RpcAuth, name='auth')    
    config.scan()
    return config.make_wsgi_app()
