from sqlalchemy.orm import (
    scoped_session,
    sessionmaker,
    )
from sqlalchemy.ext.declarative import declarative_base
from zope.sqlalchemy import ZopeTransactionExtension


DBSession = scoped_session(sessionmaker(extension=ZopeTransactionExtension()))
PbbDBSession = scoped_session(sessionmaker(extension=ZopeTransactionExtension()))
PadlDBSession = scoped_session(sessionmaker(extension=ZopeTransactionExtension()))
AuthDBSession = scoped_session(sessionmaker(extension=ZopeTransactionExtension()))

Base = declarative_base()
PbbBase = declarative_base()
PadlBase = declarative_base()

class CommonModel(object):
    def to_dict(self): # Elixir like
        values = {}
        for column in self.__table__.columns:
            values[column.name] = getattr(self, column.name)
        return values
