import sys
import xmlrpclib
import os
from hashlib import md5
from pprint import pprint


#################
# BPHTB Profile #
#################
def bphtb_profile_usage(argv):
    cmd = os.path.basename(argv[0])
    print('usage: %s <id_trx> <nop> <user_id> <password> [url]\n'
          '(example: "%s 111 12345678 admin Adm123")' % (cmd, cmd))
    sys.exit(1)

def bphtb_profile(argv=sys.argv):
    if len(argv) < 5:
        bphtb_profile_usage(argv)
    p = dict(TrxID = argv[1],
             NOP = argv[2],
             Username = argv[3],
             Password = md5(argv[4]).hexdigest())
    if argv[5:]:
        url = argv[5]
    else:
        url = 'http://localhost:6543/bphtb'
    remote = xmlrpclib.ServerProxy(url)
    print('URL: {url}'.format(url=url))
    print('Input:')
    pprint(p)
    r = remote.get_profile(p)
    print('Output:')
    pprint(r)

###########################
# BPHTB Periode Transaksi #
###########################
def bphtb_trx_usage(argv):
    cmd = os.path.basename(argv[0])
    print('usage: %s <tgl_awal> <tgl_akhir> <user_id> <password> [page] [url]\n'
          '(example: "%s 1-1-2015 31-1-2015 admin Adm123")' % (cmd, cmd))
    sys.exit(1)
    
def bphtb_trx(argv=sys.argv):
    if len(argv) < 5:
        bphtb_trx_usage(argv)
    p = dict(Awal = argv[1],
             Akhir = argv[2],
             Username = argv[3],
             Password = md5(argv[4]).hexdigest())
    url = 'http://localhost:6543/bphtb'
    if argv[5:]:
        try:
            p['Page'] = int(argv[5])
        except ValueError:
            url = argv[5]
    remote = xmlrpclib.ServerProxy(url)
    print('URL: {url}'.format(url=url))
    print('Input:')
    pprint(p)
    r = remote.get_trx(p)
    print('Output:')
    pprint(r)
    
###################
# PBB NOP Profile #
###################
def pbb_profile_usage(argv):
    cmd = os.path.basename(argv[0])
    print('usage: %s <nop> <user_id> <password> [url]\n'
          '(example: "%s 3278001001001000201994 admin Adm123")' % (cmd, cmd))
    sys.exit(1)

def pbb_profile(argv=sys.argv):
    if len(argv) < 4:
        pbb_profile_usage(argv)
    p = dict(NOP = argv[1],
             Username = argv[2],
             Password = md5(argv[3]).hexdigest())
    if argv[4:]:
        url = argv[4]
    else:
        url = 'http://localhost:6543/pbb'
    remote = xmlrpclib.ServerProxy(url)
    print('URL: {url}'.format(url=url))
    print('Input:')
    pprint(p)
    r = remote.get_profile(p)
    print('Output:')
    pprint(r)
    
#############################################
# PBB daftar NOP berdasarkan tanggal terbit #
#############################################
def pbb_profiles_usage(argv):
    cmd = os.path.basename(argv[0])
    print('usage: %s <tgl_awal> <tgl_akhir> <user_id> <password> [page] [url]\n'
          '(example: "%s 1-1-2015 31-1-2015 admin Adm123")' % (cmd, cmd))
    sys.exit(1)
    
def pbb_profiles(argv=sys.argv):
    if len(argv) < 5:
        pbb_profiles_usage(argv)
    p = dict(Awal = argv[1],
             Akhir = argv[2],
             Username = argv[3],
             Password = md5(argv[4]).hexdigest())
    url = 'http://localhost:6543/pbb'
    if argv[5:]:
        try:
            p['Page'] = int(argv[5])
        except ValueError:
            url = argv[5]
    remote = xmlrpclib.ServerProxy(url)
    print('URL: {url}'.format(url=url))
    print('Input:')
    pprint(p)
    r = remote.get_profiles(p)
    print('Output:')
    pprint(r)
    
#######################################################
# PBB daftar pembayaran NOP berdasarkan tanggal bayar #
#######################################################
def pbb_trx_usage(argv):
    cmd = os.path.basename(argv[0])
    print('usage: %s <tgl_awal> <tgl_akhir> <user_id> <password> [page] [url]\n'
          '(example: "%s 1-1-2015 31-1-2015 admin Adm123")' % (cmd, cmd))
    sys.exit(1)
    
def pbb_trx(argv=sys.argv):
    if len(argv) < 5:
        pbb_trx_usage(argv)
    p = dict(Awal = argv[1],
             Akhir = argv[2],
             Username = argv[3],
             Password = md5(argv[4]).hexdigest())
    url = 'http://localhost:6543/pbb'
    if argv[5:]:
        try:
            p['Page'] = int(argv[5])
        except ValueError:
            url = argv[5]
    remote = xmlrpclib.ServerProxy(url)
    print('URL: {url}'.format(url=url))
    print('Input:')
    pprint(p)
    r = remote.get_trx(p)
    print('Output:')
    pprint(r)
    
################
# PADL Profile #
################
def padl_profile_usage(argv):
    cmd = os.path.basename(argv[0])
    print('usage: %s <nop> <user_id> <password> [url]\n'
          '(example: "%s 201467968 admin Adm123")' % (cmd, cmd))
    sys.exit(1)

def padl_profile(argv=sys.argv):
    if len(argv) < 4:
        padl_profile_usage(argv)
    p = dict(InvoiceID = argv[1],
             Username = argv[2],
             Password = md5(argv[3]).hexdigest())
    if argv[4:]:
        url = argv[4]
    else:
        url = 'http://localhost:6543/padl'
    remote = xmlrpclib.ServerProxy(url)
    print('URL: {url}'.format(url=url))
    print('Input:')
    pprint(p)
    r = remote.get_profile(p)
    print('Output:')
    pprint(r)
    
    
###################################
# PADL berdasarkan tanggal terima #
###################################
def padl_profiles_usage(argv):
    cmd = os.path.basename(argv[0])
    print('usage: %s <tgl_awal> <tgl_akhir> <user_id> <password> [page] [url]\n'
          '(example: "%s 1-1-2015 31-1-2015 admin Adm123")' % (cmd, cmd))
    sys.exit(1)
    
def padl_profiles(argv=sys.argv):
    if len(argv) < 5:
        padl_profiles_usage(argv)
    p = dict(Awal = argv[1],
             Akhir = argv[2],
             Username = argv[3],
             Password = md5(argv[4]).hexdigest())
    url = 'http://localhost:6543/padl'
    if argv[5:]:
        try:
            p['Page'] = int(argv[5])
        except ValueError:
            url = argv[5]
    remote = xmlrpclib.ServerProxy(url)
    print('URL: {url}'.format(url=url))
    print('Input:')
    pprint(p)
    r = remote.get_profiles(p)
    print('Output:')
    pprint(r)

#########################################
# PADL daftar berdasarkan tanggal bayar #
#########################################
def padl_trx_usage(argv):
    cmd = os.path.basename(argv[0])
    print('usage: %s <tgl_awal> <tgl_akhir> <user_id> <password> [page] [url]\n'
          '(example: "%s 1-1-2015 31-1-2015 admin Adm123")' % (cmd, cmd))
    sys.exit(1)
    
def padl_trx(argv=sys.argv):
    if len(argv) < 5:
        padl_trx_usage(argv)
    p = dict(Awal = argv[1],
             Akhir = argv[2],
             Username = argv[3],
             Password = md5(argv[4]).hexdigest())
    url = 'http://localhost:6543/padl'
    if argv[5:]:
        try:
            p['Page'] = int(argv[5])
        except ValueError:
            url = argv[5]
    remote = xmlrpclib.ServerProxy(url)
    print('URL: {url}'.format(url=url))
    print('Input:')
    pprint(p)
    r = remote.get_trx(p)
    print('Output:')
    pprint(r)

########
# Auth #
########
def auth_usage(argv):
    cmd = os.path.basename(argv[0])
    print('usage: %s <user_id> <password>\n'
          '(example: "%s admin Adm123")' % (cmd, cmd))
    sys.exit(1)
    
def auth(argv=sys.argv):
    if len(argv) < 3:
        auth_usage(argv)
    p = dict(Username = argv[1],
             Password = md5(argv[2]).hexdigest())
    if argv[3:]:
        url = argv[3]
    else:
        url = 'http://localhost:6543/auth'
    remote = xmlrpclib.ServerProxy(url)
    print('URL: {url}'.format(url=url))
    print('Input:')
    pprint(p)
    r = remote.auth(p)
    print('Output:')
    pprint(r)
