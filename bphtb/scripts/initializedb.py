import os
import sys
from shutil import copyfile
from pyramid.paster import get_appsettings


def usage(argv):
    cmd = os.path.basename(argv[0])
    print('usage: %s <config_uri>\n'
          '(example: "%s development.ini")' % (cmd, cmd))
    sys.exit(1)

def main(argv=sys.argv):
    if len(argv) != 2:
        usage(argv)
    config_uri = argv[1]
    settings = get_appsettings(config_uri)
    dir_name = settings['static_files']
    mkdir(dir_name)
    contoh_files = ['xmlrpc.inc', 'pbb_profile.php']
    for contoh_file in contoh_files:
        filename = os.path.join('data', contoh_file)
        source = get_fullpath(filename)
        target = os.path.join(settings['static_files'], contoh_file)
        if not os.path.exists(target):
            copyfile(source, target)
    
def mkdir(dir_name):
    if not os.path.exists(dir_name):
        os.mkdir(dir_name)
        
def get_fullpath(filename):
    dir_name = os.path.split(__file__)[0]
    return os.path.join(dir_name, filename)    
