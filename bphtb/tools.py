from types import (
    StringType,
    UnicodeType,
    BooleanType,
    LongType,
    IntType,
    )
from datetime import (
    date,
    datetime,
    )    
from decimal import Decimal
from xmlrpclib import (
    MININT,
    MAXINT,
    )
from pyramid.threadlocal import get_current_registry    

    
kini = datetime.now()    
DateType = type(kini.date())
DateTimeType = type(kini)
TimeType = type(kini.time())
DecimalType = type(Decimal(0))

def dmy(tgl):
    return tgl.strftime('%d-%m-%Y')
    
def dmyhms(t):
    return t.strftime('%d-%m-%Y %H:%M:%S')  
    
def hms(t):
    return t.strftime('%H:%M:%S')      

def to_simple_value(v):
    typ = type(v)
    if typ is DateType:
        return dmy(v)
    if typ is DateTimeType:
        return dmyhms(v)
    if typ is TimeType:
        return hms(v)
    if typ is DecimalType:
        return float(v) 
    if typ in (LongType, IntType):
        if v < MININT or v > MAXINT:
            return str(v)
    if v == 0:
        return '0'
    if typ in [UnicodeType, StringType]:
        return v.strip()
    if v is None:
        return ''
    return v
    
def dict_to_simple_value(d):
    r = {}
    for key in d:
        val = d[key]
        if type(key) not in (UnicodeType, StringType):
            key = str(key)
        r[key] = to_simple_value(val)
    return r
    
def date_from_str(value):
    separator = None
    value = value.split()[0] # dd-mm-yyyy HH:MM:SS  
    for s in ['-', '/']:
        if value.find(s) > -1:
            separator = s
            break    
    if separator:
        t = map(lambda x: int(x), value.split(separator))
        y, m, d = t[2], t[1], t[0]
        if d > 999: # yyyy-mm-dd
            y, d = d, y
    else: # if len(value) == 8: # yyyymmdd
        y, m, d = int(value[:4]), int(value[4:6]), int(value[6:])
    return date(y, m, d)    
    
##########
# String #
##########
def clean(s):
    r = ''
    for ch in s:
        ascii = ord(ch)
        if ascii > 126 or ascii < 32:
            ch = ' '
        r += ch
    return r

def to_str(s):
    s = s or ''
    s = type(s) in [StringType, UnicodeType] and s or str(s)
    return clean(s)

def left(s, width):
    s = to_str(s)
    return s.ljust(width)[:width]

def right(s, width):
    s = to_str(s)
    return s.zfill(width)[:width]
    
##############
# Fix Length #
##############
class FixLength(object):
    def __init__(self, struct):
        self.set_struct(struct)

    def set_struct(self, struct):
        self.struct = struct
        self.fields = {}
        new_struct = []
        for s in struct:
            name = s[0]
            size = s[1:] and s[1] or 1
            typ = s[2:] and s[2] or 'A' # N: numeric, A: alphanumeric
            self.fields[name] = {'value': None, 'type': typ, 'size': size}
            new_struct.append((name, size, typ))
        self.struct = new_struct

    def set(self, name, value):
        self.fields[name]['value'] = value

    def get(self, name):
        return self.fields[name]['value']
 
    def __setitem__(self, name, value):
        self.set(name, value)        

    def __getitem__(self, name):
        return self.get(name)
 
    def get_raw(self):
        s = ''
        for name, size, typ in self.struct:
            v = self.fields[name]['value']
            pad_func = typ == 'N' and right or left
            if typ == 'N':
                v = v or 0
                i = int(v)
                if v == i:
                    v = i
            else:
                v = v or ''
            s += pad_func(v, size)
        return s

    def set_raw(self, raw):
        awal = 0
        for t in self.struct:
            name = t[0]
            size = t[1:] and t[1] or 1
            akhir = awal + size
            value = raw[awal:akhir]
            if not value:
                return
            self.set(name, value)
            awal += size
        return True

    def from_dict(self, d):
        for name in d:
            value = d[name]
            self.set(name, value)
    

############
# Database #
############
def split_tablename(tablename):
    t = tablename.split('.')
    if t[1:]:
        schema = t[0]
        tablename = t[1]
    else:
        schema = None
    return schema, tablename

###########
# Pyramid #
###########
def get_settings():
    reg = get_current_registry()
    return reg.settings
