from types import (
    StringType,
    UnicodeType,
    FloatType,
    IntType,
    )
from hashlib import md5
from sqlalchemy import (
    func,
    Table,
    Column,
    Integer,
    Float,
    DateTime,
    )
from sqlalchemy.sql.expression import text 
from pyramid.view import view_config
from pyramid_xmlrpc import XMLRPCView
from .models import (
    DBSession,
    PbbDBSession,
    PadlDBSession,
    AuthDBSession,
    Base,
    PbbBase,
    PadlBase,
    CommonModel,
    )
from .tools import (
    dict_to_simple_value,
    date_from_str,
    FixLength,
    get_settings,
    split_tablename,
    DateType,
    DateTimeType,
    )

LIMIT = 1000
CODE_OK = 0
CODE_NOT_FOUND = -1
CODE_INVALID_LOGIN = -10

PBB_INVOICE_DESCRIPTION = dict(
    kd_propinsi	= 'Bagian dari NOP.',
    kd_dati2 = 'Bagian dari NOP.',
    kd_kecamatan = 'Bagian dari NOP.',
    kd_kelurahan = 'Bagian dari NOP.',
    kd_blok = 'Bagian dari NOP.',
    no_urut = 'Bagian dari NOP.',
    kd_jns_op = 'Bagian dari NOP.',
    thn_pajak_sppt = 'Bagian dari NOP.',
    pbb_yg_harus_dibayar_sppt = 'Nilai yang harus dibayar.',
    siklus_sppt = 'Misalkan bernilai 5 berarti telah 5 kali ditetapkan.',
    )
PBB_PAYMENT_DESCRIPTION = dict(
    kd_propinsi	= 'Bagian dari NOP.',
    kd_dati2 = 'Bagian dari NOP.',
    kd_kecamatan = 'Bagian dari NOP.',
    kd_kelurahan = 'Bagian dari NOP.',
    kd_blok = 'Bagian dari NOP.',
    no_urut = 'Bagian dari NOP.',
    kd_jns_op = 'Bagian dari NOP.',
    thn_pajak_sppt = 'Bagian dari NOP.',
    pembayaran_sppt_ke = 'Sebuah NOP bisa saja beberapa kali pembayaran '\
        'dimana field ini adalah urutannya.',
    jml_sppt_yg_dibayar = 'Jumlah bruto pembayaran oleh wajib pajak, '\
        'sehingga pokok = jml_sppt_yg_dibayar - denda_sppt.',
    denda_sppt = 'Denda yang dibebankan atas keterlambatan pembayaran.',
    )

@view_config(route_name='home', renderer='templates/mytemplate.pt')
def my_view(request):
    settings = get_settings()
    is_bphtb = DBSession.bind != None
    is_pbb = PbbDBSession.bind != None
    is_padl = PadlDBSession.bind != None
    r = dict(is_bphtb=is_bphtb, is_pbb=is_pbb, is_padl=is_padl,
            title=settings.web_title)
    if is_pbb:
        r['pbb_invoice'] = table_structure(PbbBase.metadata,
                            settings.pbb_invoice_table)
        r['pbb_payment'] = table_structure(PbbBase.metadata,
                            settings.pbb_payment_table)
        r['pbb_invoice_desc'] = PBB_INVOICE_DESCRIPTION
        r['pbb_payment_desc'] = PBB_PAYMENT_DESCRIPTION
    return r

def get_offset(p):
    if 'Page' in p:
        return (p['Page'] - 1) * LIMIT
    return 0

def check_offset(d, offset, page_count):
    if offset >= 0:
        d['page_count'] = page_count
    return d


############
# Base ORM #
############
class BaseORM(object): # abstract
    def __init__(self, db_base, db_session, tablename):
        db_base = db_base
        self.db_session = db_session
        schema, tablename = split_tablename(tablename)
        args = dict(autoload=True)
        if schema:
            args['schema'] = schema

        class cls(db_base, CommonModel):
            __tablename__ = tablename 
            __table_args__ = args

        self.cls = cls

    def get_count(self, q):
        r = q.first()
        page_count = r.jml / LIMIT
        if r.jml % LIMIT > 0:
            page_count += 1
        return r.jml, page_count

    def get_rows(self, q, offset):
        if offset < 0:
            return q
        return q.offset(offset).limit(LIMIT)


#########
# BPHTB #
#########
class BphtbORM(BaseORM): # abstract
    def __init__(self, tablename):
        BaseORM.__init__(self, Base, DBSession, tablename)
        

class BphtbInvoice(BphtbORM):
    def __init__(self):
        self.settings = get_settings()
        BphtbORM.__init__(self, self.settings.bphtb_invoice_table)
        
    def get_profile(self, transno, nop):
        q = DBSession.query(self.cls).filter_by(transno=transno, nop=nop)
        return q.first()
        
        
class BphtbPayment(BphtbORM):
    def __init__(self):
        self.settings = get_settings()
        BphtbORM.__init__(self, self.settings.bphtb_payment_table)
        
    def get_count(self, awal, akhir):
        field_tanggal = getattr(self.cls, self.settings.bphtb_payment_date)
        q = DBSession.query(func.count().label('jml')).filter(
                field_tanggal.between(awal, akhir))
        return BaseORM.get_count(self, q)
        
    def get_rows(self, awal, akhir, offset):
        field_tanggal = getattr(self.cls, self.settings.bphtb_payment_date)    
        q = DBSession.query(self.cls).filter(
                field_tanggal.between(awal, akhir))
        q = q.order_by('id')
        return BaseORM.get_rows(self, q, offset)


class RpcBPHTB(XMLRPCView):
    def get_profile(self, params):
        resp = auth_from_rpc(params)
        if resp['code'] != 0:
            return resp    
        bphtb = BphtbInvoice()
        row = bphtb.get_profile(params['TrxID'],params['NOP'])
        if not row:
            return dict(code=CODE_NOT_FOUND,
                        message='Transaksi NOP {nop} tidak ada'.format(nop=params['NOP']))
        r = dict(code=0, message='OK')
        vals = row.to_dict()
        vals = dict_to_simple_value(vals)
        r.update(vals)
        return r
        
    def get_trx(self, params):
        resp = auth_from_rpc(params)
        if resp['code'] != 0:
            return resp    
        awal = date_from_str(params['Awal'])
        akhir = date_from_str(params['Akhir'])
        bphtb = BphtbPayment()
        row_count, page_count = bphtb.get_count(awal, akhir)
        t
        if not row_count:
            msg = 'Tidak ada transaksi di periode {awal} and {akhir}'
            msg = msg.format(awal=params['Awal'], akhir=params['Akhir'])
            return dict(code=CODE_NOT_FOUND, message=msg)
        offset = get_offset(params)
        rows = bphtb.get_rows(awal, akhir, offset)
        r = dict(code=0, message='OK')
        trxs = []
        for row in rows:
            vals = row.to_dict()
            vals = dict_to_simple_value(vals)
            trxs.append(vals)
        r['rows'] = trxs
        return check_offset(r, offset, page_count)

#######        
# PBB #
#######
class PbbORM(BaseORM): # abstract
    def __init__(self, tablename):
        BaseORM.__init__(self, PbbBase, PbbDBSession, tablename)


class PbbInvoice(PbbORM):
    def __init__(self):
        settings = get_settings()
        PbbORM.__init__(self, settings.pbb_invoice_table)

    def get_profile(self, propinsi, kabupaten, kecamatan, kelurahan, blok, urut,
        jenis, tahun):
        q = PbbDBSession.query(self.cls).filter_by(kd_propinsi=propinsi,
               kd_dati2=kabupaten, kd_kecamatan=kecamatan,
               kd_kelurahan=kelurahan, kd_blok=blok, no_urut=urut,
               kd_jns_op=jenis, thn_pajak_sppt=tahun)
        return q.first()

    def get_count(self, awal, akhir):
        q = PbbDBSession.query(func.count().label('jml')).filter(
                self.cls.tgl_terbit_sppt.between(awal, akhir))
        return PbbORM.get_count(self, q)

    def get_rows(self, awal, akhir, offset=0):
        q = PbbDBSession.query(self.cls).filter(
                self.cls.tgl_terbit_sppt.between(awal, akhir))
        q = q.order_by('kd_propinsi', 'kd_dati2', 'kd_kecamatan', 'kd_kelurahan',
                'kd_blok', 'no_urut', 'kd_jns_op', 'thn_pajak_sppt')
        return PbbORM.get_rows(self, q, offset)


class PbbPayment(PbbORM):
    def __init__(self, awal, akhir):
        settings = get_settings()
        PbbORM.__init__(self, settings.pbb_payment_table)
        self.awal = awal
        self.akhir = akhir

    def get_count(self):
        q = PbbDBSession.query(func.count().label('jml')).filter(
                self.cls.tgl_rekam_byr_sppt.between(self.awal, self.akhir))
        return PbbORM.get_count(self, q)

    def get_rows(self, offset=0):
        q = PbbDBSession.query(self.cls).filter(
                self.cls.tgl_rekam_byr_sppt.between(self.awal, self.akhir))
        q = q.order_by('kd_propinsi', 'kd_dati2', 'kd_kecamatan', 'kd_kelurahan',
                'kd_blok', 'no_urut', 'kd_jns_op', 'thn_pajak_sppt',
                'pembayaran_sppt_ke')
        return PbbORM.get_rows(self, q, offset)


PBB_INVOICE_ID = [
    ('Propinsi', 2, 'N'),
    ('Kabupaten', 2, 'N'),
    ('Kecamatan', 3, 'N'),
    ('Kelurahan', 3, 'N'),
    ('Blok', 3, 'N'),
    ('Urut', 4, 'N'),
    ('Jenis', 1, 'N'),
    ('Tahun Pajak', 4, 'N'),
    ]
        
class RpcPBB(XMLRPCView):
    def get_profile(self, params):
        resp = auth_from_rpc(params)
        if resp['code'] != 0:
            return resp    
        invoice_id = FixLength(PBB_INVOICE_ID)
        invoice_id.set_raw(params['NOP'])
        inv = PbbInvoice()
        row = inv.get_profile(invoice_id['Propinsi'], invoice_id['Kabupaten'],
                invoice_id['Kecamatan'], invoice_id['Kelurahan'],
                invoice_id['Blok'], invoice_id['Urut'], invoice_id['Jenis'],
                invoice_id['Tahun Pajak'])
        if not row:
            return dict(code=CODE_NOT_FOUND,
                        message='NOP {nop} tidak ada'.format(nop=params['NOP']))
        r = dict(code=0, message='OK')
        vals = row.to_dict()
        vals = dict_to_simple_value(vals)
        r.update(vals)
        return r

    def get_profiles(self, params):
        resp = auth_from_rpc(params)
        if resp['code'] != 0:
            return resp    
        awal = date_from_str(params['Awal'])
        akhir = date_from_str(params['Akhir'])
        inv = PbbInvoice()
        row_count, page_count = inv.get_count(awal, akhir)
        if not row_count:
            msg = 'Tidak ada NOP di periode {awal} and {akhir}'
            msg = msg.format(awal=params['Awal'], akhir=params['Akhir'])
            return dict(code=CODE_NOT_FOUND, message=msg)
        offset = get_offset(params)
        rows = inv.get_rows(awal, akhir, offset)
        r = dict(code=0, message='OK')
        trxs = []
        for row in rows:
            vals = row.to_dict()
            vals = dict_to_simple_value(vals)
            trxs.append(vals)
        r['rows'] = trxs
        return check_offset(r, offset, page_count)

    def get_trx(self, params):
        resp = auth_from_rpc(params)
        if resp['code'] != 0:
            return resp
        awal = date_from_str(params['Awal'])
        akhir = date_from_str(params['Akhir'])
        pay = PbbPayment(awal, akhir)
        row_count, page_count = pay.get_count()
        if not row_count:
            msg = 'Tidak ada pembayaran di periode {awal} and {akhir}'
            msg = msg.format(awal=params['Awal'], akhir=params['Akhir'])
            return dict(code=CODE_NOT_FOUND, message=msg)
        offset = get_offset(params)
        rows = pay.get_rows(offset)
        r = dict(code=0, message='OK')
        trxs = []
        for row in rows:
            vals = row.to_dict()
            vals = dict_to_simple_value(vals)
            trxs.append(vals)
        r['rows'] = trxs
        return check_offset(r, offset, page_count)

########
# PADL #
########
class PadlORM(BaseORM): # abstract
    def __init__(self, tablename):
        BaseORM.__init__(self, PadlBase, PadlDBSession, tablename)

        
class PadlInvoice(PadlORM):
    def __init__(self):
        settings = get_settings()
        PadlORM.__init__(self, settings.padl_invoice_table)
        
    def get_profile(self, tahun, sptno):
        q = PadlDBSession.query(self.cls).filter_by(tahun=tahun, sptno=sptno)
        return q.first()
        
    def get_count(self, awal, akhir):
        q = PadlDBSession.query(func.count().label('jml')).filter(
                self.cls.terimatgl.between(awal, akhir))
        return PadlORM.get_count(self, q)
    
    def get_rows(self, awal, akhir, offset):
        q = PadlDBSession.query(self.cls).filter(
                self.cls.terimatgl.between(awal, akhir))
        q = q.order_by('id')
        return PadlORM.get_rows(self, q, offset)


class PadlPayment(PadlORM):
    def __init__(self, awal, akhir):
        self.awal = awal
        self.akhir = akhir    
        settings = get_settings()
        PadlORM.__init__(self, settings.padl_payment_table)

    def get_count(self):
        q = PadlDBSession.query(func.count().label('jml')).filter(
                self.cls.sspdtgl.between(self.awal, self.akhir))
        return PadlORM.get_count(self, q)
    
    def get_rows(self, offset):
        q = PadlDBSession.query(self.cls).filter(
                self.cls.sspdtgl.between(self.awal, self.akhir))
        q = q.order_by('id')
        return PadlORM.get_rows(self, q, offset)


def padl_decode_invoice_id_raw(raw):
    tahun = raw[:4]
    sptno = raw[4:10]
    return tahun, sptno
    
                   
class RpcPADL(XMLRPCView):
    def get_profile(self, params):
        resp = auth_from_rpc(params)
        if resp['code'] != 0:
            return resp    
        tahun, sptno = padl_decode_invoice_id_raw(params['InvoiceID'])
        padl = PadlInvoice()
        row = padl.get_profile(tahun, sptno)
        if not row:
            return dict(code=CODE_NOT_FOUND,
                        message='InvoiceID {id} tidak ada'.format(
                            id=params['InvoiceID']))
        r = dict(code=0, message='OK')
        vals = row.to_dict()
        vals = dict_to_simple_value(vals)
        r.update(vals)
        return r

    def get_profiles(self, params):
        resp = auth_from_rpc(params)
        if resp['code'] != 0:
            return resp    
        awal = date_from_str(params['Awal'])
        akhir = date_from_str(params['Akhir'])
        padl = PadlInvoice()
        row_count, page_count = padl.get_count(awal, akhir)
        if not row_count:
            msg = 'Tidak ada NOP di periode {awal} and {akhir}'
            msg = msg.format(awal=params['Awal'], akhir=params['Akhir'])
            return dict(code=CODE_NOT_FOUND, message=msg)
        offset = get_offset(params)
        rows = padl.get_rows(awal, akhir, offset)
        r = dict(code=0, message='OK')
        trxs = []
        for row in rows:
            vals = row.to_dict()
            vals = dict_to_simple_value(vals)
            trxs.append(vals)
        r['rows'] = trxs
        return check_offset(r, offset, page_count)
        
    def get_trx(self, params):
        resp = auth_from_rpc(params)
        if resp['code'] != 0:
            return resp
        awal = date_from_str(params['Awal'])
        akhir = date_from_str(params['Akhir'])
        padl = PadlPayment(awal, akhir)
        row_count, page_count = padl.get_count()
        if not row_count:
            msg = 'Tidak ada pembayaran di periode {awal} and {akhir}'
            msg = msg.format(awal=params['Awal'], akhir=params['Akhir'])
            return dict(code=CODE_NOT_FOUND, message=msg)
        offset = get_offset(params)
        rows = padl.get_rows(offset)
        r = dict(code=0, message='OK')
        trxs = []
        for row in rows:
            vals = row.to_dict()
            vals = dict_to_simple_value(vals)
            trxs.append(vals)
        r['rows'] = trxs
        return check_offset(r, offset, page_count)


########        
# Auth #
########
def auth(username, password):
    settings = get_settings()
    sql = "SELECT {password} AS password FROM {table} WHERE {id} = :id"
    sql = sql.format(table=settings.auth_table, id=settings.auth_key_field,
            password=settings.auth_password_field)
    engine = AuthDBSession.bind
    q = engine.execute(text(sql), id = username)
    user = q.first()
    if not user:
        return
    if settings.auth_password_type == 'plain':
        password_md5 = md5(user.password).hexdigest()
    else:
        password_md5 = user.password
    if password == password_md5:
        return user
        
def auth_from_rpc(params):
    user = auth(params['Username'], params['Password'])
    if user:
        return dict(code=CODE_OK, message='OK')
    return dict(code=CODE_INVALID_LOGIN, message='Gagal login')
            
class RpcAuth(XMLRPCView):
    def auth(self, params):
        return auth_from_rpc(params)

###################        
# Table Structure #
###################
def change_column_def(c):
    if hasattr(c.type, 'precision'):
        if c.type.precision > 10:
            return Column(Float, nullable=c.nullable, name=c.name,
                    primary_key=c.primary_key)
        return Column(Integer, nullable=c.nullable, name=c.name,
                primary_key=c.primary_key)
    if hasattr(c.server_default, 'arg') and \
        hasattr(c.server_default.arg, 'text') and \
        c.server_default.arg.text.strip().lower() == 'sysdate': # Oracle
        return Column(DateTime, nullable=c.nullable, name=c.name,
            default=func.now)
    return c
        
def column_info(c):
    if c.primary_key:
        other = 'key'
    elif c.nullable:
        other = 'nullable'
    else:
        other = ''
    size = ''
    if c.type.python_type in (StringType, UnicodeType):
        typ = 'string'
        size = c.type.length
    elif c.type.python_type == IntType:
        typ = 'integer'
    elif c.type.python_type == FloatType:
        typ = 'float'
    elif c.type.python_type == DateType:
        typ = 'date'
    elif c.type.python_type == DateTimeType:
        typ = 'datetime'
    else:
        typ = c.type
    return (c.name, typ, size, other)

def table_structure(metadata, tablename):
    settings = get_settings()
    schema, tablename = split_tablename(tablename)
    t = Table(tablename, metadata, autoload=True, schema=schema)
    r = dict(table = tablename)
    cols = []
    for c in t.columns:
        c = change_column_def(c)
        info = column_info(c)
        cols.append(info)
    r['columns'] = cols
    return r
