import os
import subprocess
import sys
from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(here, 'README.txt')) as f:
    README = f.read()
with open(os.path.join(here, 'CHANGES.txt')) as f:
    CHANGES = f.read()

requires = [
    'pyramid',
    'pyramid_chameleon',
    'pyramid_debugtoolbar',
    'pyramid_tm',
    'SQLAlchemy',
    'transaction',
    'zope.sqlalchemy',
    'waitress',
    'pyramid_xmlrpc',
    'psycopg2',
    ]

if sys.argv[1:] and sys.argv[1] == 'develop-use-pip':
    bin_ = os.path.split(sys.executable)[0]
    pip = os.path.join(bin_, 'pip')
    for package in requires:
        cmd = [pip, 'install', package]
        subprocess.call(cmd)
    cmd = [sys.executable, sys.argv[0], 'develop']
    subprocess.call(cmd)
    sys.exit()

setup(name='bphtb',
      version='0.0',
      description='bphtb',
      long_description=README + '\n\n' + CHANGES,
      classifiers=[
        "Programming Language :: Python",
        "Framework :: Pyramid",
        "Topic :: Internet :: WWW/HTTP",
        "Topic :: Internet :: WWW/HTTP :: WSGI :: Application",
        ],
      author='',
      author_email='',
      url='',
      keywords='web wsgi bfg pylons pyramid',
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      test_suite='bphtb',
      install_requires=requires,
      entry_points="""\
      [paste.app_factory]
      main = bphtb:main
      [console_scripts]
      initialize_db = bphtb.scripts.initializedb:main      
      bphtb_profile = bphtb.scripts.TestClient:bphtb_profile
      bphtb_trx = bphtb.scripts.TestClient:bphtb_trx
      pbb_profile = bphtb.scripts.TestClient:pbb_profile
      pbb_profiles = bphtb.scripts.TestClient:pbb_profiles
      pbb_trx = bphtb.scripts.TestClient:pbb_trx
      padl_profile = bphtb.scripts.TestClient:padl_profile
      padl_profiles = bphtb.scripts.TestClient:padl_profiles
      padl_trx = bphtb.scripts.TestClient:padl_trx
      auth = bphtb.scripts.TestClient:auth
      """,
      )
